@extends('master')
@section("content")
<!-- <div class="container custom_product">
  <div class="carousel-inner">
    @foreach($products as $item)
        <div class="item {{$item['id']==1?'active':''}}">
            <img src="{{$item['gallery']}}">
        </div>
    @endforeach
     
  </div>
</div> -->
<div class="container custom_product">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
            @foreach($products as $item)
                <div class="item {{$item['id']==1?'active':''}}">
                <img src="{{$item['gallery']}}" >
                </div>
                <div class="carousel-item">
                <img src="/asset/img/proios/gggUntitled-1-1024x360.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="/asset/img/proios/new--1024x360.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="/asset/img/proios/new-1-1-1024x360.jpg" class="d-block w-100" alt="...">
                </div>
            @endforeach
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>  
</div>
@endsection