<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Products')->insert([
            [
                'name' => 'IPhone 12',
                "price"=>"1300",
                "description"=>"Apple iPhone 12 256GB Blue",
                "category"=>"Mobile",
                "gallery"=>"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvyJ9EBF81ONUkTun3IkgOIwCyp5V0LP1aw8V1S2wT9O04qj8MCFWHhstE3lSH2aWGW-s&usqp=CAU"
            
            ],
            [
                'name' => 'IPhone 12 Pro',
                "price"=>"1500",
                "description"=>"A IOSphone",
                "category"=>"Mobile",
                "gallery"=>"https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/iphone-12-pro-blue-hero?wid=470&hei=556&fmt=png-alpha&.v=1604021661000"
            
            ]

        ]);
    }
}
